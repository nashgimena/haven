$(document).ready(function(){

	// Day dropdown
	let day = $('#day-dropdown');

	const url_day = 'assets/json/day.json ';

	$.getJSON(url_day, function (data) {
	  $.each(data, function (key, entry) {
		day.append($('<option></option>').attr('value', entry.abbreviation).text(entry.name));
	  })
	});

	// Time dropdown
	let time = $('#time-dropdown');

	const url_time = 'assets/json/time.json ';

	$.getJSON(url_time, function (data) {
	  $.each(data, function (key, entry) {
		time.append($('<option></option>').attr('value', entry.abbreviation).text(entry.name));
	  })
	});

	$('.contact-number').keyup(function(){     
	});

$('#contact-form').submit(function(e){     

	var $form = $(this);
	var dropday = $('#day-dropdown').val();
	var droptime = $('#time-dropdown').val();

	if(!$.isNumeric($('.contact-number').val())) {
		$('.contact-form-div .number-error').show();
		e.preventDefault();
	} else {
		$('.contact-form-div .number-error').hide();
	}

	if (!dropday || dropday == '') {
		$('.contact-form-div .day-error').show();
		e.preventDefault();
	} else {
		$('.contact-form-div .day-error').hide();
	}

	if (!droptime || droptime == '') {
		$('.contact-form-div .time-error').show();
		e.preventDefault();
	} else {
		$('.contact-form-div .time-error').hide();
	}

	
});


});